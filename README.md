# BranksMod

Currently in beta, BranksMod is a complete rewrite of the original BakkesModInjector with new and improved features.

![](preview.png)

## Contact

If you notice any problems or have a suggestion feel free to message me on Discord - ItsBranK#4095

Also be sure to join our Discord server.

https://discordapp.com/invite/HsM6kAR

![](Invite.png)